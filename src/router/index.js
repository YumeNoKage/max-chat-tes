import Vue from 'vue'
import VueRouter from 'vue-router'
import AllPhone from '../views/Home.vue'
import Samsung from '../views/Samsung.vue'
import Iphone from '../views/Iphone.vue'
import JsonShowcase from '../views/JsonShowcase.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'AllPhone',
    component: AllPhone
  },
  {
    path: '/samsung',
    name: 'Samsung',
    component: Samsung
  },
  {
    path: '/iphone',
    name: 'IPhone',
    component: Iphone
  },
  {
    path: '/json-showcase',
    name: 'JsonShowcase',
    component: JsonShowcase
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
