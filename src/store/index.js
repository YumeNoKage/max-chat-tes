import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    complete: [
      {"id": 1, "name": "iPhone 6", "brand": "Apple", "price": 190, "color": "White"},
      {"id": 2, "name": "iPhone 6 Plus", "brand": "Apple", "price": 260, "color": "Black"},
      {"id": 3, "name": "iPhone 6 Plus", "brand": "Apple", "price": 260, "color": "White"},
      {"id": 4, "name": "iPhone 5", "brand": "Apple", "price": 200, "color": "Red"},
      {"id": 5, "name": "Galaxy S5", "brand": "Samsung", "price": 210, "color": "Orange"},
      {"id": 6, "name": "Galaxy S6", "brand": "Samsung", "price": 230, "color": "Orange"},
      {"id": 7, "name": "Galaxy S6", "brand": "Samsung", "price": 250, "color": "Red"}
    ],
    summary: { 
      'Samsung': 3,
      'Apple': 4 
    },
    colors: ["White", "Black", "Red", "Orange"],
    unique: [
      {"id": 1, "name": "iPhone 6", "brand": "Apple", "price": 190, "color": "White"}, 
      {"id": 2, "name": "iPhone 6 Plus", "brand": "Apple", "price": 260, "color": "Black"},
      {"id": 4, "name": "iPhone 5", "brand": "Apple", "price": 200, "color": "Red"},
      {"id": 5, "name": "Galaxy S5", "brand": "Samsung", "price": 210, "color": "Orange"},
      {"id": 6, "name": "Galaxy S6", "brand": "Samsung", "price": 230, "color": "Orange"},
    ]
  },

  getters:{
    complete: state => state.complete,
    summary: state => state.summary,
    color: state => state.colors,
    unique: state => state.unique,
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
